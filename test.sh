#!/bin/sh

result=0

for i in "$DATADIR"/*.in
do
	input=$i
	output=${i%.in}.out
	echo Testing: `basename $input`...
	"$EXE" < "$input" 2> /dev/null | diff -su "$output" -

	if [ $? -ne 0 ]
	then
		result=1
	fi
done

exit $result
